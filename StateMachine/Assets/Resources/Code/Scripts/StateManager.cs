﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;
using Assets.Resources.Code.Scripts;
using System.IO.Ports;
using System.Threading;
using System;

/// <summary>
/// State manager class. The core of the satte machine
/// </summary>
/// <remarks>this is the core of the state machine we use this to manage flow of the game logic
/// this class objective are:
/// - Delegate the control to the active state using Update and OnGui function
/// - Switching to the new state when required using SwitchSatte
/// - Memmorize current state in activeState variable
/// </remarks>
public class StateManager : MonoBehaviour {

    /// <summary>
    /// variable to store the active state
    /// </summary>
    private IStateBase activeState;

    /* 
     */
    /// <summary>
    /// game data reference
    /// </summary>
    /// <remarks>prevent showing this variable in Inspector of Unity
    /// In our case GameData store only data about game and so there is no
    /// reason to allow it changeable in the inspector</remarks>
    [HideInInspector]
    public GameData gameDataRef;

    /// <summary>
    /// game manager class
    /// </summary>
    private static StateManager instanceRef;

    /// <summary>
    /// Create variable to open Arduino comunication
    /// </summary>
    public SerialPort sp = new SerialPort("COM1", 9600);

    /// <summary>
    /// create thread to read asynchronously Arduino output
    /// </summary>
    Thread arduinoThread;

    /// <summary>
    /// method recalled one time right after scene is created. after object create and before game plays
    ///  </summary>
    void Awake()
    {
        /* if StateManger already exist destroy immediatly this object, unless create it 
         * and don't destroy it during all game. We want only one game maner class and create
         * it when we start the game
         */
        if (instanceRef == null)
        {
            instanceRef = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            // when BeginningScene is reloaded we destroy the new GamaManager cause we want only the original one
            DestroyImmediate(gameObject);
        }
    }

    /// <summary>
    /// Use this for initialization
    /// </summary>
    void Start()
    {
        // setting up the beginning state
        activeState = new BeginState(this);

        //take refrence to game data only on times during game
        gameDataRef = GetComponent<GameData>();

        // try open Arduino comunication
        sp.Open();

        // create and start thread to manage Arduino data
        arduinoThread = new Thread(new ThreadStart(GetArduino));
        arduinoThread.Start();
    }

    /// <summary>
    /// function to read Arduino output data and set the value in GameData class
    /// </summary>
    private void GetArduino()
    {
        // while thread is active 
        while (arduinoThread.IsAlive)
        {
            // read line output
            string value = sp.ReadLine();
            // convert arduino string output to int and assign to the boolean value in GameData class the right value
            if (Convert.ToInt16(value[0]) - 48 == 0) gameDataRef.ArduinoButtonA = true;
            else gameDataRef.ArduinoButtonA = false;
            if (Convert.ToInt16(value[1]) - 48 == 0) gameDataRef.ArduinoButtonB = true;
            else gameDataRef.ArduinoButtonB = false;
        }
    }

    /// <summary>
    /// close serial port comunication with Arduino if you close your program
    /// </summary>
    void OnDestroy()
    {
        // Close the port when the program ends.
        if (sp.IsOpen)
        {
            try
            {
                sp.Close();
            }
            catch (UnityException e)
            {
                Debug.LogError(e.Message);
            }
        }
    }

    /// <summary>
    /// Update is called once per frame. delegate the update to the active state
    /// </summary>
    void Update()
    {
        if (activeState != null)
        {
            activeState.StateUpdate();
        }
        Debug.Log("I'm in update function");
    }

    /// <summary>
    /// Show text button etc. Is called at least one per frame but could be more
    /// </summary>
    void OnGUI()
    {
        // if one state is active draw active state GUI
        if (activeState != null)
        {
            activeState.ShowIt();
        }
    }

    /// <summary>
    /// Switch from one state to other
    /// </summary>
    public void SwitchState(IStateBase newSate_tmp)
    {
        // switch to new state
        activeState = newSate_tmp;
    }

    /// <summary>
    /// Destroy and reload Scene00
    /// </summary>
    public void Restart()
    {
        // destroy all object also GameManager because we want to restart from scratch ( from zero )
        Destroy(gameObject);
        // load scene 00
        Application.LoadLevel("Scene00");
    }
}
