﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;
using Assets.Resources.Code.Scripts;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Play state. when you play the game you are here. in this class you manage all the logic of the game during playing
    /// </summary>
    /// <remarks>TO DO: put here a detailed description of this class and logic
    /// </remarks>
    class PlayState : IStateBase
    {
        /// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;
        /// <summary>
        /// raference to the shooter object
        /// </summary>
        private GameObject shooter;
        /// <summary>
        /// memorize here the script attached to the shooter object
        /// </summary>
        public Shooter shooterScript;
        /// <summary>
        /// reference to the force bar object
        /// </summary>
        private GameObject forceBar;
        /// <summary>
        /// memorize here the script attached to the force bar object
        /// </summary>
        public CastingBar forceBarScript;
        /// <summary>
        /// reference to the countdown timer object
        /// </summary>
        private GameObject clockTimer;
        /// <summary>
        /// memorize here the script attached the timer
        /// </summary>
        public ClockScript clockScript;

        /// <summary>
        /// Create the state and load the Scene01
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        /// <remarks>when initialize play state we also take the reference to the player object and to 
        /// objects for the force management ( force bar and clock timer )</remarks>
        public PlayState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class
         
            // Load Scene01
            if (Application.loadedLevelName != "Scene01")
            {
                Application.LoadLevel("Scene01");
            }

            shooter = GameObject.Find("Shooter");   // find Shooter object in the Scene01

            shooterScript = shooter.GetComponent<Shooter>();    // assign shooter script

            forceBar = GameObject.Find("CastBar");  // find CastBar object

            forceBarScript = forceBar.GetComponent<CastingBar>();   // assign CastingBar script

            forceBarScript.InitializeForceBar(manager.gameDataRef.forceManager.ForceLowerLimit, manager.gameDataRef.forceManager.ForceUpperLimit, manager.gameDataRef.forceManager.actualForce);    // initialize force bar value

            clockTimer = GameObject.Find("alarmClockBlue(Clone)"); // find ClockTimer object

            clockScript = clockTimer.GetComponent<ClockScript>();   // assign clock timer script

            clockTimer.SetActive(false);    // hide the object at the start. we show it only if we are in threshold

        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        /// <remarks>TO DO: put here your play state update function detailed description
        /// ...
        /// We also use a block of code to manage/simulate the force in the if mouse condition we use the A input key to decremnt the force value
        /// the B key to incrment the force value. and the simultaneus pressure on this keys to mantain the force in the constant value
        /// we also manage the activation of the clock timer</remarks>
        public void StateUpdate()
        {
            // if mouse is present update the force value
            if (Input.mousePresent)
            {
                // if Arduino connected simulate force with it
                if (manager.sp.IsOpen)
                {                
                    // pass to the function the bool value to simulate decrease and increase of force
                    manager.gameDataRef.forceManager.UpdateForce(manager.gameDataRef.ArduinoButtonA, manager.gameDataRef.ArduinoButtonB);
                }
                // pass to the function the bool value to simulate decrease and increase of force using keyboard
                else  manager.gameDataRef.forceManager.UpdateForce(Input.GetKey(KeyCode.A), Input.GetKey(KeyCode.B));

                // if timer is active update it
                if (manager.gameDataRef.forceManager.IsTimerActive)
                {
                    float actualRemainingTime = manager.gameDataRef.forceManager.remainingTime;
                    shooterScript.UpdateShooter(manager.gameDataRef.forceManager.updateTimer());
                    clockScript.UpdateClock((manager.gameDataRef.forceManager.remainingTime - actualRemainingTime ) * 360 / manager.gameDataRef.forceManager.RemainingTimeSetted);
                }
                // else if I'm in threshold and at costant value start it
                else if (manager.gameDataRef.forceManager.InThresholdValue == ForceManager.InThreshold.inner && manager.gameDataRef.forceManager.InStdDevForveLimit)
                {
                    manager.gameDataRef.forceManager.StartTimer();
                    clockTimer.SetActive(true);
                }
                
                forceBarScript.UpdateCastBar(manager.gameDataRef.forceManager.actualForce); // set the actual force value in the bar
                forceBarScript.UpdateCastBarColor((int)manager.gameDataRef.forceManager.InThresholdValue); // update force bar color
            }        

            // if timer is inactive hide the clock timer and reset it
            if (!manager.gameDataRef.forceManager.IsTimerActive)
            {
                clockTimer.SetActive(false);
                clockScript.restArrow();
            }
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <remarks>TO DO:
        /// if you use also this function for the logic you need to specify here what appened</remarks>
        public void ShowIt()
        {
            // draw the texture store in instructionStateSplash variable that is the size of the screen
            GUI.DrawTexture(new Rect(Screen.width / 2 - 256 / 2, Screen.height / 2 - 128 / 2, 256, 128), manager.gameDataRef.playStateSplash, ScaleMode.StretchToFill);
            
            // show timer of force bar and std deviation of force
            GUI.Box(new Rect(Screen.width / 2 - 160 / 2, Screen.height / 2, 160, 120), "Play state/ timer:= " + manager.gameDataRef.forceManager.remainingTime + " StdDev" + manager.gameDataRef.forceManager.StdDevForce);

            GUI.backgroundColor = Color.white;
            // if button pressed or W keys
            if (GUI.Button(new Rect(10, 25, 250, 60), "Press Here or Any W to Won") || Input.GetKeyUp(KeyCode.W))
            {
                // switch the state to Setup state
                manager.SwitchState(new WonState(manager));
            }

            // if button pressed or L keys
            if (GUI.Button(new Rect(Screen.width/2 - 125, 25, 250, 60), "Press Here or Any L to Lose") || Input.GetKeyUp(KeyCode.L))
            {
                // switch the state to Setup state
                manager.SwitchState(new LostState(manager));
            }

            // if button pressed or R keys
            if (GUI.Button(new Rect(Screen.width - 250 - 5, 25, 250, 60), "Press Here or Any R to Return") || Input.GetKeyUp(KeyCode.R))
            {
                // switch the state to Setup state
                manager.SwitchState(new SetupState(manager));
            }
            Debug.Log("In PlayState");
        }
    }
}